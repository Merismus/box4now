//
//  HomeVC.swift
//  Box4Now
//
//  Created by Olaf Bergmann on 23/11/2018.
//  Copyright © 2018 Olaf Bergmann. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import RevealingSplashView
import Firebase

enum AnnotationType {
    case pickup
    case destination
    case courier
}

enum ButtonAction {
    case requestDelivery
    case getDirectionToUser
    case getDirectionToDestination
    case startDelivery
    case endDelivery
}

class HomeVC: UIViewController, MKMapViewDelegate {

    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var actionButton: RoundedShadowButton!
    @IBOutlet weak var centerMapBtn: UIButton!
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var destinationCircle: CircleView!
    @IBOutlet weak var cancelBtn: UIButton!
    
    
    struct UserAndCurierAreConnected {
        static var userAndCurierAreConnected = false
    }
    
    var delegate: CenterVCDelegate?
    
    var manager = CLLocationManager()
    
    var regionRadius: CLLocationDistance = 1000
    
    let testCordinates = CLLocationCoordinate2D(latitude: 0.24, longitude: 0.55)
    
    let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "Box4Now2")!, iconInitialSize: CGSize(width: 80, height: 80), backgroundColor: UIColor.white)
    
    var tableView = UITableView()
    
    var matchingItems: [MKMapItem] = [MKMapItem]()
    
    var route: MKRoute!
    
    var selectedItemPlacemark: MKPlacemark? = nil
    
    var actionForButton: ButtonAction = .requestDelivery
    
    var currentUserIdd = Auth.auth().currentUser?.uid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ////
        currentUserIdd = Auth.auth().currentUser?.uid
        
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        
        checkLocationAuthStatus()
        
        mapView?.delegate = self
        destinationTextField.delegate = self
        
        centerMapOnUserLocation()
        
        DataService.instance.REF_COURIERS.observe(.value, with: { (snapshot) in
            self.loadCourierAnnotationsFromFB()
            
            let currentUserId = Auth.auth().currentUser?.uid
            guard currentUserId == nil else {
                DataService.instance.userIsOnTrip(userKey: currentUserId!, handler: { (isOnTrip, courierKey, tripKey) in
                    if isOnTrip == true {
                        self.zoom(toFitAnnotationsFromMapView: self.mapView, forActiveTripWithCourier: true, withKey: courierKey)
                    }
                })
                return
            }

        })
        
        cancelBtn.alpha = 0.0
        
        self.view.addSubview(revealingSplashView)
        revealingSplashView.animationType = SplashAnimationType.heartBeat
        revealingSplashView.startAnimation()

        UpdateService.instance.observeTrips { (tripDict) in
            if let tripDict = tripDict {
                let pickupCoordinateArray = tripDict["pickupCoordinate"] as! NSArray
                let destinationCoordinateArray = tripDict["destinationCoordinate"] as! NSArray
                let tripKey = tripDict["userKey"] as! String
                let acceptanceStatus = tripDict["tripIsAccepted"] as! Bool
                
                if acceptanceStatus == false {
                    let currentUserId = Auth.auth().currentUser?.uid
                    if currentUserId != nil { 
                        DataService.instance.driverIsAvailable(key: currentUserId!, handler: { (available) in
                            if let available = available {
                                if available == true {
                                    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                                    let pickupVC = storyboard.instantiateViewController(withIdentifier: "PickupVC") as? PickupVC
                                    pickupVC?.initData(pickupCoordinate: CLLocationCoordinate2D(latitude: pickupCoordinateArray[0] as! CLLocationDegrees, longitude: pickupCoordinateArray[1] as! CLLocationDegrees), destinationCoordinate: CLLocationCoordinate2D(latitude: destinationCoordinateArray[0] as! CLLocationDegrees, longitude: destinationCoordinateArray[1] as! CLLocationDegrees), userKey: tripKey)
                                    self.present(pickupVC!, animated: true, completion: nil)

                                }
                            }
                        })
                    }

                    
                }
            }
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let currentUserId = Auth.auth().currentUser?.uid
        guard currentUserId == nil else {
        
            DataService.instance.userIsCourier(userKey: currentUserId!, handler: { (status) in
                if status == true {
                    self.buttonForCourier(areHidden: true)
                } else {
                    self.buttonForCourier(areHidden: false)
                }
            })
            
            DataService.instance.REF_TRIPS.observe(.childRemoved, with: { (removedTripSnapshot) in
                let removedTripDict = removedTripSnapshot.value as? [String: AnyObject]
                if removedTripDict?["courierKey"] != nil {
                    DataService.instance.REF_COURIERS.child(removedTripDict?["courierKey"] as! String).updateChildValues(["courierIsOnTrip": false])
                    
                    DataService.instance.userIsCourier(userKey: currentUserId!, handler: { (status) in
                        if status == true {
                            self.buttonForCourier(areHidden: true)
                        } else {
                            self.buttonForCourier(areHidden: false)
                        }
                    })
                }
                
            DataService.instance.userIsCourier(userKey: currentUserId!, handler: { (isCourier) in
                if isCourier == true {
                    self.removeOverLaysAndAnnotations(forCouriers: false, forUsers: true)
                    self.cancelBtn.fadeTo(alphaValue: 0.0, withDuration: 0.2)
                } else {

                    self.actionButton.animateButton(shouldLoad: false, withMessage: "ORDER")
                        
                    self.destinationTextField.isUserInteractionEnabled = true
                    self.destinationTextField.text = ""
                        
                    self.removeOverLaysAndAnnotations(forCouriers: false, forUsers: true)
                    self.centerMapOnUserLocation()
                }
            })
        })
            
            DataService.instance.courierIsOnTrip(courierKey: currentUserId!, handler: { (isOnTrip, courierKey, tripKey) in
                if isOnTrip == true {
                    DataService.instance.REF_TRIPS.observeSingleEvent(of: .value, with: { (tripSnapshot) in
                        if let tripSnapshot = tripSnapshot.children.allObjects as? [DataSnapshot] {
                            for trip in tripSnapshot {
                                if trip.childSnapshot(forPath: "courierKey").value as? String == currentUserId {
                                    let pickupCoordinateArray = trip.childSnapshot(forPath: "pickupCoordinate").value as! NSArray
                                    let pickupCoordinate = CLLocationCoordinate2D(latitude: pickupCoordinateArray[0] as! CLLocationDegrees, longitude: pickupCoordinateArray[1] as! CLLocationDegrees)
                                    let pickupPlacemark = MKPlacemark(coordinate: pickupCoordinate)
                                    self.dropPinFor(placemark: pickupPlacemark)
                                    self.searchMapKitForResultsWithPolyLine(forOriginMapItem: nil, withDestinationMapItem: MKMapItem(placemark: pickupPlacemark))

                                    self.setCustomRegion(forAnnotationType: .pickup, withCoordinate: pickupCoordinate)

                                    self.actionForButton = .getDirectionToUser
                                    self.actionButton.setTitle("GET DIRECTIONS", for: .normal)

                                    self.buttonForCourier(areHidden: false)
                                }
                            }
                        }
                    })
                }
            })
            
            connectUserAndCourierForTrip()
            return
        }
        
}
    
    func checkLocationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            //manager.delegate = self
            //manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.startUpdatingLocation()
        } else {
            manager.requestAlwaysAuthorization()
        }
    }
    
    func buttonForCourier(areHidden: Bool) {
        if areHidden {
            self.actionButton.fadeTo(alphaValue: 0.0, withDuration: 0.2)
            self.cancelBtn.fadeTo(alphaValue: 0.0, withDuration: 0.2)
            self.centerMapBtn.fadeTo(alphaValue: 0.0, withDuration: 0.2)
            self.actionButton.isHidden = true
            self.cancelBtn.isHidden = true
            self.centerMapBtn.isHidden = true
        } else {
            self.actionButton.fadeTo(alphaValue: 1.0, withDuration: 0.2)
            self.cancelBtn.fadeTo(alphaValue: 1.0, withDuration: 0.2)
            self.centerMapBtn.fadeTo(alphaValue: 1.0, withDuration: 0.2)
            self.actionButton.isHidden = false
            self.cancelBtn.isHidden = false
            self.centerMapBtn.isHidden = false
        }
    }
    
    func loadCourierAnnotationsFromFB() {
        DataService.instance.REF_COURIERS.observeSingleEvent(of: .value, with: { (snapshot) in
            if let courierSnapshot = snapshot.children.allObjects as? [DataSnapshot] {
                for courier in courierSnapshot {
                    if courier.hasChild("userIsCourier") {
                        if courier.hasChild("coordinate") {
                            if courier.childSnapshot(forPath: "isPickupModeEnabled").value as? Bool == true {
                                if let courierDict = courier.value as? Dictionary<String, AnyObject> {
                                    let coordinateArray = courierDict["coordinate"] as! NSArray
                                    let courierCoordinate = CLLocationCoordinate2D(latitude: coordinateArray[0] as! CLLocationDegrees, longitude: coordinateArray[1] as! CLLocationDegrees)
                                    
                                    let annotation = CourierAnnotation(coordinate: courierCoordinate, withKey: courier.key)
                                    var courierIsVisible: Bool {
                                        return self.mapView.annotations.contains(where: { (annotation) -> Bool in
                                            if let courierAnnotation = annotation as? CourierAnnotation {
                                                if courierAnnotation.key == courier.key {
                                                    courierAnnotation.update(annotationPosition: courierAnnotation, withCoordinate: courierCoordinate)
                                                    return true
                                                }
                                            }
                                            return false
                                        })
                                    }
                                    if !courierIsVisible {
                                        self.mapView.addAnnotation(annotation)
                                    }
                                }
                            } else {
                                for annotation in self.mapView.annotations {
                                    if annotation.isKind(of: CourierAnnotation.self) {
                                        if let annotation = annotation as? CourierAnnotation {
                                            if annotation.key == courier.key {
                                                self.mapView.removeAnnotation(annotation)
                                            }
                                        }
                                    }
                                        
                                }
                            }
                        }
                    }
                   
                }
            }
        })
        revealingSplashView.heartAttack = true
    }
    
    func connectUserAndCourierForTrip() {
        let currentUserId = Auth.auth().currentUser?.uid
        guard currentUserIdd == nil else {
        DataService.instance.userIsOnTrip(userKey: currentUserIdd!, handler: { (isOnTrip, courierKey, tripKey) in
                if isOnTrip == true {
                    self.removeOverLaysAndAnnotations(forCouriers: false, forUsers: true)
                    
                    DataService.instance.REF_TRIPS.child(tripKey!).observeSingleEvent(of: .value, with: { (tripSnapshot) in
                        let tripDict = tripSnapshot.value as? Dictionary<String, AnyObject>
                        let courierId = tripDict?["courierKey"] as! String
                        let pickupCoodinateArray = tripDict?["pickupCoordinate"] as! NSArray
                        let pickupCoordinate = CLLocationCoordinate2D(latitude: pickupCoodinateArray[0] as! CLLocationDegrees, longitude: pickupCoodinateArray[1] as! CLLocationDegrees)
                        let pickupPlacemark = MKPlacemark(coordinate: pickupCoordinate)
                        let pickupMapItem = MKMapItem(placemark: pickupPlacemark)
                        
                    DataService.instance.REF_COURIERS.child(courierId).child("coordinate").observeSingleEvent(of: .value, with: { (coordinateSnapshot) in
                        let coordinateSnapshot = coordinateSnapshot.value as! NSArray
                        let courierCoordinate = CLLocationCoordinate2D(latitude: coordinateSnapshot[0] as! CLLocationDegrees, longitude: coordinateSnapshot[1] as! CLLocationDegrees)
                        let courierPlacemark = MKPlacemark(coordinate: courierCoordinate)
                        let courierMapItem = MKMapItem(placemark: courierPlacemark)
                        let userAnnotation = PackageAnnotation(coordinate: pickupCoordinate, key: currentUserId!)
                        self.mapView.addAnnotation(userAnnotation)
                        self.searchMapKitForResultsWithPolyLine(forOriginMapItem: courierMapItem, withDestinationMapItem: pickupMapItem)
                        self.actionButton.animateButton(shouldLoad: false, withMessage: "COURIER IS COMING")
                        self.actionButton.isUserInteractionEnabled = false
                        })
                        
                        DataService.instance.REF_TRIPS.child(tripKey!).observeSingleEvent(of: .value, with: { (tripSnapshot) in
                            if tripDict?["deliveryIsInProgress"] as? Bool == true {
                                self.removeOverLaysAndAnnotations(forCouriers: true, forUsers: true)
                                let destinationCoordinateArray = tripDict?["destinationCoordinate"] as! NSArray
                                let destinationCoordinate = CLLocationCoordinate2D(latitude: destinationCoordinateArray[0] as! CLLocationDegrees, longitude: destinationCoordinateArray[1] as! CLLocationDegrees)
                                let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate)
                                self.dropPinFor(placemark: destinationPlacemark)
                                self.searchMapKitForResultsWithPolyLine(forOriginMapItem: pickupMapItem, withDestinationMapItem: MKMapItem(placemark: destinationPlacemark))

                                self.actionButton.setTitle("ON TRIP", for: .normal)
                            }
                        })
                    })
                }
            })
            return
       }
    }
    
    func centerMapOnUserLocation() {
        let cordinateRegion = MKCoordinateRegion(center: mapView?.userLocation.coordinate ?? testCordinates, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        mapView?.setRegion(cordinateRegion, animated: true)
    }
    
    @IBAction func actionButtonWasPressed(_ sender: Any) {
        buttonSelector(forAction: actionForButton)
    }

    @IBAction func cancelBtnWasPressed(_ sender: Any) {
        //sprawdzanie czy paczka została już odebrana
        let currentUserId = Auth.auth().currentUser?.uid
        guard currentUserIdd == nil else {
            DataService.instance.courierIsOnTrip(courierKey: currentUserId!) { (isOnTrip, courierKey, tripKey) in
                if isOnTrip == true {
                    UpdateService.instance.cancelTrip(withPackageKey: tripKey!, forCourierKey: courierKey!)
                }
            }
            DataService.instance.userIsOnTrip(userKey: currentUserId!) { (isOnTrip, courierKey, tripKey) in
                if isOnTrip == true {
                    
                    UpdateService.instance.cancelTrip(withPackageKey: currentUserId!, forCourierKey: courierKey!)
                    UserAndCurierAreConnected.userAndCurierAreConnected = false

                } else {
                    UpdateService.instance.cancelTrip(withPackageKey: currentUserId!, forCourierKey: nil)
                    self.removeOverLaysAndAnnotations(forCouriers: false, forUsers: true)
                    self.centerMapOnUserLocation()
                }
            }
            self.removeOverLaysAndAnnotations(forCouriers: false, forUsers: true)
            self.actionButton.isUserInteractionEnabled = true
            return
        }
    }
    
    @IBAction func menuButtonWasPressed(_ sender: Any) {
        //self.view.endEditing(true)
        delegate?.toggleLeftPanel()
    }
    
    func buttonSelector(forAction action: ButtonAction) {
        switch action {
        case .requestDelivery:
            if Auth.auth().currentUser?.uid == nil {
                showAlert("Please create account first, you can do it in left up corner menu")
            } else if destinationTextField.text!.isEmpty {
                showAlert("Destination can't be empty")
            } else {
                UpdateService.instance.updateTripsWithCoordinatesUponRequest()
                actionButton.animateButton(shouldLoad: true, withMessage: nil)
                self.view.endEditing(true)
                destinationTextField.isUserInteractionEnabled = false
            }
        case .getDirectionToUser:
            let currentUserId = Auth.auth().currentUser?.uid
            guard currentUserId == nil else {
                DataService.instance.courierIsOnTrip(courierKey: currentUserId!,handler:  { (isOnTrip, courierKey, tripKey) in
                    if isOnTrip == true {
                        DataService.instance.REF_TRIPS.child(tripKey!).observe(.value, with: {(tripSnapshot) in
                            let tripDict = tripSnapshot.value as? Dictionary<String, AnyObject>
                            let pickupCoordinateArray = tripDict?["pickupCoordinate"] as! NSArray
                            let pickupCoordinate = CLLocationCoordinate2D(latitude: pickupCoordinateArray[0] as! CLLocationDegrees, longitude: pickupCoordinateArray[1] as! CLLocationDegrees)
                            let pickupMapItem = MKMapItem(placemark: MKPlacemark(coordinate: pickupCoordinate))
                            pickupMapItem.name = "Package Pickup Point"
                            pickupMapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving])
                        })
                    }
                })
                return
            }
            
        case .startDelivery:
            let currentUserId = Auth.auth().currentUser?.uid
            guard currentUserId == nil else {
                
                DataService.instance.courierIsOnTrip(courierKey: currentUserId!, handler: { (isOnTrip, courierKey, tripKey) in
                    if isOnTrip == true {
                        self.removeOverLaysAndAnnotations(forCouriers: false, forUsers: false)
                        DataService.instance.REF_TRIPS.child(tripKey!).updateChildValues(["deliveryIsInProgress": true])
                        DataService.instance.REF_TRIPS.child(tripKey!).child("destinationCoordinate").observeSingleEvent(of: .value, with: { (coordinateSnapshot) in
                            let destinationCoordinateArray = coordinateSnapshot.value as! NSArray
                            let destinationCoordinate = CLLocationCoordinate2D(latitude: destinationCoordinateArray[0] as! CLLocationDegrees, longitude: destinationCoordinateArray[1] as! CLLocationDegrees)
                            let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate)
                            self.dropPinFor(placemark: destinationPlacemark)
                            self.searchMapKitForResultsWithPolyLine(forOriginMapItem: nil, withDestinationMapItem: MKMapItem(placemark: destinationPlacemark))
                            self.setCustomRegion(forAnnotationType: .destination, withCoordinate: destinationCoordinate)
                            self.actionForButton = .getDirectionToDestination
                            self.actionButton.setTitle("GET DIRECTIONS", for: .normal)
                        })
                    }
                })
                return
            }

        case .getDirectionToDestination:
            let currentUserId = Auth.auth().currentUser?.uid
            guard currentUserId == nil else {
                DataService.instance.courierIsOnTrip(courierKey: currentUserId!) { (isOnTrip, courierKey, tripKey) in
                    if isOnTrip == true {
                        DataService.instance.REF_TRIPS.child(tripKey!).child("destinationCoordinate").observe(.value, with: { (snapshot) in
                            let destinationCoordinateArray = snapshot.value as! NSArray
                            let destinationCoordinate = CLLocationCoordinate2D(latitude: destinationCoordinateArray[0] as! CLLocationDegrees, longitude: destinationCoordinateArray[1] as! CLLocationDegrees)
                            let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate)
                            let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
                            destinationMapItem.name = "Package Destination"
                            destinationMapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving])
                        })
                    }
                }
            return
            }
            
        case.endDelivery:
            let currentUserId = Auth.auth().currentUser?.uid
            guard currentUserId == nil else {
                DataService.instance.courierIsOnTrip(courierKey: currentUserId!) { (isOnTrip, courierKey, tripKey) in
                    if isOnTrip == true {
                        UpdateService.instance.cancelTrip(withPackageKey: tripKey!, forCourierKey: courierKey)
                        self.buttonForCourier(areHidden: true)
                        UserAndCurierAreConnected.userAndCurierAreConnected = false
                    }
                }
            return
            }
        }
    }
    
    
    @IBAction func centerMapBtnWasPressed(_ sender: Any) {
        DataService.instance.REF_USERS.observeSingleEvent(of: .value) { (snapshot) in
            if let userSnapshot = snapshot.children.allObjects as? [DataSnapshot] {
                for user in userSnapshot {
                    if user.key == Auth.auth().currentUser?.uid {
                        if user.hasChild("tripCoordinate") {
                            self.zoom(toFitAnnotationsFromMapView: self.mapView, forActiveTripWithCourier: false, withKey: nil)
                            self.centerMapBtn.fadeTo(alphaValue: 0.0, withDuration: 0.2)
                        } else {
                            self.centerMapOnUserLocation()
                            self.centerMapBtn.fadeTo(alphaValue: 0.0, withDuration: 0.2)
                        }
                    } else {
                        self.centerMapOnUserLocation()
                        self.centerMapBtn.fadeTo(alphaValue: 0.0, withDuration: 0.2)
                    }
                }
            }
        }
    }
    
}
