# Box4Now

Application Box4Now was part of my thesis.

The subject of this thesis it was to implement fully functional mobile application Box4Now for iOS platform,
which let their user to send, pick up and deliver packages in the city and provide him clear and user-friendly graphical interface.
Application is implemented in Swift programing language and real time database is serviced by Firebase platform. Except of send,
pick up and carry packages it let user log in, register, recover a password and change basic parameters of account. Application is dedicated
for all devices with version 12 of iOS installed.
